#TwoDroids

The TwoBulls Android resource library for (y)our convenience.  

###Usage

In the root `buidle.gradle` file add:  

	allprojects {
		repositories {
			...
			maven { url "https://jitpack.io" }
		}
	}
	
Add as a dependency in the module/app `build.gradle`:  

	dependencies {
		compile 'org.bitbucket.twobulls.twodroids:<module>:<Tag|Commit-Hash>'
	}

Or more advanced/customized:  

	compile ('org.bitbucket.twobulls.twodroids:two-ui:<Tag|Commit>') {
        exclude module: 'two-common'
    }


####Modules

- two-common
- two-ui
- two-testing


###Contribute

If you have a reusable component and want to reuse it different projects but don't want to copy/paste? Feel free to add it to one of the existing module or create a new one if none of the existing modules is applicable.  
For contributions please checkout `git@bitbucket.org:twobulls/twodroids.git` and create a feature branch using:  

- [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/)  
- Create tests where appropriate  
- Run `./gradlew test checkstyle` before committing changes on the master branch  

###Changelog

0.1.0 First Release
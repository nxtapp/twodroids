package com.twodroids.ui.helper;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Collection;

/**
 * Created by mensly on 6/01/2016.
 */
public final class ContentDescriber implements View.OnLongClickListener {
    public static final ContentDescriber INSTANCE = new ContentDescriber();

    @SuppressWarnings("HardCodedStringLiteral")
    public static void bind(View view) {
        if (view.getContentDescription() == null || view.getContentDescription().length() == 0) {
            Log.w("ContentDescriber", "Content describer added to a view without a content description: " + view);
        }
        view.setOnLongClickListener(INSTANCE);
    }

    public static void bindAll(Collection<? extends View> views) {
        for (View v : views) {
            bind(v);
        }
    }

    public static void bindChildren(ViewGroup viewGroup) {
        final int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view.getContentDescription() != null) {
                bind(view);
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        CharSequence contentDescription = v.getContentDescription();
        if (contentDescription != null) {
            Toast.makeText(v.getContext(), contentDescription, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}

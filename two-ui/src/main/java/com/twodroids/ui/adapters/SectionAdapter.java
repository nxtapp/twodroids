package com.twodroids.ui.adapters;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

/**
 * Simplifies concatenating many separate adapters, each with their own (optional) header
 * Created by mensly on 12/02/2016.
 */
public class SectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Section> sections = new ArrayList<>();
    private final WeakHashMap<RecyclerView.ViewHolder, RecyclerView.Adapter> parentAdapter = new WeakHashMap<>();
    private RecyclerView recyclerView;
    private WeakReference<OnHeaderInflatedListener> onHeaderCreatedListener = new WeakReference<>(null);

    public SectionAdapter() {
        setHasStableIds(true);
    }

    public OnHeaderInflatedListener getOnHeaderCreatedListener() {
        return onHeaderCreatedListener.get();
    }

    public void setOnHeaderCreatedListener(OnHeaderInflatedListener onHeaderCreatedListener) {
        this.onHeaderCreatedListener = new WeakReference<>(onHeaderCreatedListener);
    }

    public void addSection(@LayoutRes int headerLayout, RecyclerView.Adapter adapter, int viewTypeCount) {
        int oldCount = getItemCount();
        Section section = new Section(headerLayout, adapter, viewTypeCount);
        sections.add(section);
        adapter.registerAdapterDataObserver(new Observer(section));
        if (!adapter.hasStableIds()) {
            setHasStableIds(false);
        }
        if (recyclerView != null) {
            adapter.onAttachedToRecyclerView(recyclerView);
        }
        int added = adapter.getItemCount();
        if (headerLayout != 0) {
            added++;
        }
        notifyItemRangeInserted(oldCount, added);
    }

    @Override
    public long getItemId(int position) {
        for (Section section : sections) {
            if (section.headerLayout != 0) {
                if (position == 0) {
                    return RecyclerView.NO_ID;
                }
                position--;
            }
            int count = section.adapter.getItemCount();
            if (position < count) {
                return section.adapter.getItemId(position);
            }
            position -= count;
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public int getItemViewType(int position) {
        int previousTypes = 0;
        for (Section section : sections) {
            if (section.headerLayout != 0) {
                if (position == 0) {
                    return previousTypes;
                }
                position--;
                previousTypes++;
            }
            int count = section.adapter.getItemCount();
            if (position < count) {
                return previousTypes + section.adapter.getItemViewType(position);
            }
            position -= count;
            previousTypes += section.viewTypeCount;
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        for (Section section : sections) {
            if (section.headerLayout != 0) {
                if (viewType == 0) {
                    View headerView = LayoutInflater.from(parent.getContext())
                            .inflate(section.headerLayout, parent, false);
                    OnHeaderInflatedListener listener = getOnHeaderCreatedListener();
                    if (listener != null) {
                        listener.onHeaderInflated(section.adapter, headerView);
                    }
                    return new HeaderViewHolder(headerView);
                }
                viewType--;
            }
            if (viewType < section.viewTypeCount) {
                RecyclerView.ViewHolder viewHolder = section.adapter.onCreateViewHolder(parent, viewType);
                parentAdapter.put(viewHolder, section.adapter);
                return viewHolder;
            }
            viewType -= section.viewTypeCount;
        }
        throw new IllegalArgumentException("viewType");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        for (Section section : sections) {
            if (section.headerLayout != 0) {
                if (position == 0) {
                    return;
                }
                position--;
            }
            int count = section.adapter.getItemCount();
            if (position < count) {
                section.adapter.onBindViewHolder(holder, position);
                return;
            }
            position -= count;
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        for (Section section : sections) {
            if (section.headerLayout != 0) {
                count++;
            }
            count += section.adapter.getItemCount();
        }
        return count;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        RecyclerView.Adapter adp = parentAdapter.get(holder);
        if (adp != null) {
            adp.onViewAttachedToWindow(holder);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        RecyclerView.Adapter adp = parentAdapter.get(holder);
        if (adp != null) {
            adp.onViewDetachedFromWindow(holder);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        RecyclerView.Adapter adp = parentAdapter.get(holder);
        if (adp != null) {
            adp.onViewRecycled(holder);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        for (Section section : sections) {
            section.adapter.onAttachedToRecyclerView(recyclerView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
        for (Section section : sections) {
            section.adapter.onDetachedFromRecyclerView(recyclerView);
        }
    }

    private static final class Section {
        final int headerLayout;
        final RecyclerView.Adapter adapter;
        final int viewTypeCount;

        public Section(int headerLayout, RecyclerView.Adapter adapter, int viewTypeCount) {
            this.headerLayout = headerLayout;
            this.adapter = adapter;
            this.viewTypeCount = viewTypeCount;
        }
    }
    private static final class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }
    private final class Observer extends RecyclerView.AdapterDataObserver {
        private final WeakReference<Section> section;

        public Observer(Section section) {
            this.section = new WeakReference<>(section);
        }

        @Override
        public void onChanged() {
            notifyDataSetChanged();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            Section section = this.section.get();
            if (section == null) { return; }
            int start = 0;
            for (Section previous : sections) {
                if (previous.headerLayout != 0) {
                    start++;
                }
                if (previous == section) {
                    fromPosition += start;
                    toPosition += start;
                    for (int i = 0; i < itemCount; i++) {
                        fromPosition++;
                        toPosition++;
                        notifyItemMoved(fromPosition, toPosition);
                    }
                    break;
                }
                start += previous.adapter.getItemCount();
            }
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            Section section = this.section.get();
            if (section == null) { return; }
            int start = 0;
            for (Section previous : sections) {
                if (previous.headerLayout != 0) {
                    start++;
                }
                if (previous == section) {
                    notifyItemRangeChanged(start + positionStart, itemCount);
                    break;
                }
                start += previous.adapter.getItemCount();
            }
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            Section section = this.section.get();
            if (section == null) { return; }
            int start = 0;
            for (Section previous : sections) {
                if (previous.headerLayout != 0) {
                    start++;
                }
                if (previous == section) {
                    notifyItemRangeInserted(start + positionStart, itemCount);
                    break;
                }
                start += previous.adapter.getItemCount();
            }
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            Section section = this.section.get();
            if (section == null) { return; }
            int start = 0;
            for (Section previous : sections) {
                if (previous.headerLayout != 0) {
                    start++;
                }
                if (previous == section) {
                    notifyItemRangeRemoved(start + positionStart, itemCount);
                    break;
                }
                start += previous.adapter.getItemCount();
            }
        }
    }

    public interface OnHeaderInflatedListener {
        void onHeaderInflated(RecyclerView.Adapter sectionAdapter, View sectionHeader);
    }
}

package com.twodroids.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

/**
 * Adapter for simple listing
 * Created by mensly on 27/08/2015.
 */
public class SimpleAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnItemSelectedListener {
        void onItemSelected(int index, Object item);
    }

    public static class StaticViewHolder extends RecyclerView.ViewHolder {
        public StaticViewHolder(View itemView) {
            super(itemView);
        }
    }

    protected static class ItemViewHolder<T> extends RecyclerView.ViewHolder {
        private T item;

        public ItemViewHolder(View itemView) {
            super(itemView);
        }

        public final void setItem(T item) {
            this.item = item;
            displayItem(item);
        }

        public T getItem() {
            return item;
        }

        protected void displayItem(T item) {
            String text = item == null ? "" : item.toString();
            if (itemView instanceof TextView) {
                ((TextView) itemView).setText(text);
            }
            else {
                TextView textView = (TextView)itemView.findViewById(android.R.id.text1);
                if (textView != null) {
                    textView.setText(text);
                }
            }
        }
    }

    private enum ViewType {
        Normal, Header, Footer, Empty;
        private static final ViewType[] values = values();
        public static ViewType valueOf(int ordinal) {
            return values[ordinal];
        }
    }

    private final int cellLayout;
    private final View header;
    private final View footer;
    private final int emptyRes;
    private List<T> items = Collections.emptyList();
    private WeakReference<OnItemSelectedListener> listener;

    public SimpleAdapter(int cellLayout, View header, View footer, int emptyRes) {
        this.cellLayout = cellLayout;
        this.header = header;
        this.footer = footer;
        this.emptyRes = emptyRes;
        setHasStableIds(true);
    }

    public OnItemSelectedListener getItemSelectedListener() {
        return listener == null ? null : listener.get();
    }

    public void setItemSelectedListener(OnItemSelectedListener listener) {
        this.listener = new WeakReference<>(listener);
    }

    public void setItems(@NonNull List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public List<T> getItems() {
        return Collections.unmodifiableList(items);
    }

    @Override
    public int getItemViewType(int position) {
        if (header != null) {
            if (position == 0) {
                return ViewType.Header.ordinal();
            }
            position--;
        }
        if (position == 0 && emptyRes != 0 && items.size() == 0) {
            return ViewType.Empty.ordinal();
        }
        if (footer != null && position >= items.size()) {
            return ViewType.Footer.ordinal();
        }
        return ViewType.Normal.ordinal();
    }

    @Override
    public long getItemId(int position) {
        switch (ViewType.valueOf(getItemViewType(position))) {
            case Header:
                return -1;
            case Footer:
                return -2;
            case Empty:
                return -3;
            case Normal:
            default:
                return getItemId(getItem(position));
        }
    }

    protected long getItemId(T item) {
        return item.hashCode();
    }

    private T getItem(int position) {
        if (header != null) {
            position--;
        }
        return items.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (ViewType.valueOf(viewType)) {
            case Header:
                return new StaticViewHolder(header);
            case Footer:
                return new StaticViewHolder(footer);
            case Empty:
                return new StaticViewHolder(inflater.inflate(emptyRes, parent, false));
            case Normal:
            default:
                ItemViewHolder<T> viewHolder = createHolder(inflater.inflate(cellLayout, parent, false));
                View button = viewHolder.itemView;
                button.setOnClickListener(clickListener);
                button.setTag(new WeakReference<>(viewHolder));
                return viewHolder;
        }
    }

    protected ItemViewHolder<T> createHolder(View itemView) {
        return new ItemViewHolder<>(itemView);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ((ItemViewHolder) holder).setItem(getItem(position));
        }
    }

    @Override
    public int getItemCount() {
        int count = items.size();
        if (count == 0 && emptyRes != 0) {
            // Supply empty view if no items
            count = 1;
        }
        if (header != null) {
            // Supply header view
            count++;
        }
        if (footer != null) {
            // Supply footer view
            count++;
        }
        return count;
    }

    protected void onItemSelected(T item) {
        OnItemSelectedListener listener = getItemSelectedListener();
        if (listener != null) {
            listener.onItemSelected(items.indexOf(item), item);
        }
    }

    private final View.OnClickListener clickListener = new View.OnClickListener() {
        @SuppressWarnings("unchecked")
        @Override
        public void onClick(View buttonView) {
            Object tag = buttonView.getTag();
            if (tag instanceof WeakReference) {
                tag = ((WeakReference) tag).get();
            }
            if (tag instanceof ItemViewHolder) {
                onItemSelected((T) ((ItemViewHolder) tag).getItem());
            }
        }
    };
}

package com.twodroids.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import static android.widget.ExpandableListView.PACKED_POSITION_TYPE_CHILD;
import static android.widget.ExpandableListView.PACKED_POSITION_TYPE_GROUP;
import static android.widget.ExpandableListView.PACKED_POSITION_TYPE_NULL;
import static android.widget.ExpandableListView.PACKED_POSITION_VALUE_NULL;
import static android.widget.ExpandableListView.getPackedPositionChild;
import static android.widget.ExpandableListView.getPackedPositionForChild;
import static android.widget.ExpandableListView.getPackedPositionForGroup;
import static android.widget.ExpandableListView.getPackedPositionGroup;
import static android.widget.ExpandableListView.getPackedPositionType;

/**
 * Recreates the functionality of ExpandableListView for use with RecyclerView
 * Created by mensly on 29/02/2016.
 */
public abstract class ExpandableAdapter<GroupVH extends RecyclerView.ViewHolder, ChildVH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private SparseBooleanArray expanded = new SparseBooleanArray(1);

    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 0) {
            return onCreateGroupViewHolder(inflater, parent);
        }
        else {
            return onCreateViewHolder(inflater, parent, viewType - 1);
        }
    }

    public long getPackedPosition(int position) {
        final int groupCount = getGroupCount();
        for (int group = 0; group < groupCount; group++) {
            if (position == 0) {
                return getPackedPositionForGroup(group);
            }
            position--;
            if (isExpanded(group)) {
                int childCount = getChildCount(group);
                if (position < childCount) {
                    return getPackedPositionForChild(group, position);
                }
                position -= childCount;
            }
        }
        return PACKED_POSITION_VALUE_NULL;
    }

    public int getPositionForGroup(int groupPosition) {
        int position = 0;
        for (int group = 0; group < groupPosition; group++) {
            position++; // Offset a header
            if (isExpanded(group)) {
                position += getChildCount(group); // Offset the children of this group
            }
        }
        return position;
    }

    public int getPosition(long packedPosition) {
        int groupPosition = getPackedPositionGroup(packedPosition);
        int position = 0;
        for (int group = 0; group < groupPosition; group++) {
            position++; // Offset a header
            if (isExpanded(group)) {
                position += getChildCount(group); // Offset the children of this group
            }
        }
        switch (getPackedPositionType(packedPosition)) {
            case PACKED_POSITION_TYPE_CHILD:
                position++; // Offset the header of *this* group
                position += getPackedPositionChild(packedPosition); // Offset previous siblings
                break;
            case PACKED_POSITION_TYPE_NULL:
                throw new IllegalArgumentException("packedPosition = " + packedPosition);

        }
        return position;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        long packedPosition = getPackedPosition(position);
        switch (getPackedPositionType(packedPosition)) {
            case PACKED_POSITION_TYPE_GROUP:
                int groupPosition = getPackedPositionGroup(packedPosition);
                onBindGroupViewHolder((GroupVH)holder, groupPosition, isExpanded(groupPosition));
                break;
            case PACKED_POSITION_TYPE_CHILD:
                onBindViewHolder((ChildVH)holder,
                        getPackedPositionGroup(packedPosition), getPackedPositionChild(packedPosition));
                break;
        }
    }

    @Override
    public final int getItemViewType(int position) {
        long packedPosition = getPackedPosition(position);
        switch (getPackedPositionType(packedPosition)) {
            case PACKED_POSITION_TYPE_GROUP:
                return 0;
            case PACKED_POSITION_TYPE_CHILD:
                return 1 + getChildViewType(getPackedPositionGroup(packedPosition), getPackedPositionChild(packedPosition));
            default:
                throw new IndexOutOfBoundsException(String.format("position = %d (> %d)", position, getItemCount()));
        }
    }

    @Override
    public final long getItemId(int position) {
        if (!hasStableIds()) {
            // No point in calculating IDs
            return RecyclerView.NO_ID;
        }

        long packedPosition = getPackedPosition(position);
        switch (getPackedPositionType(packedPosition)) {
            case PACKED_POSITION_TYPE_GROUP:
                return getGroupId(getPackedPositionGroup(packedPosition));
            case PACKED_POSITION_TYPE_CHILD:
                return getChildId(getPackedPositionGroup(packedPosition), getPackedPositionChild(packedPosition));
            default:
                throw new IndexOutOfBoundsException(String.format("position = %d (> %d)", position, getItemCount()));
        }
    }

    protected long getGroupId(int groupPosition) {
        return RecyclerView.NO_ID;
    }

    protected long getChildId(int groupPosition, int childPosition) {
        return RecyclerView.NO_ID;
    }

    @Override
    public int getItemCount() {
        final int groupCount = getGroupCount();
        int itemCount = 0;
        for (int group = 0; group < groupCount; group++) {
            itemCount++;
            if (isExpanded(group)) {
                itemCount += getChildCount(group);
            }
        }
        return itemCount;
    }

    public final boolean isExpanded(int groupPosition) {
        return this.expanded.get(groupPosition);
    }

    public void setExpanded(int groupPosition, boolean expanded) {
        final int position = getPositionForGroup(groupPosition);
        final int childCount = getChildCount(groupPosition);
        if (expanded) {
            this.expanded.put(groupPosition, true);
            notifyItemChanged(position);
            notifyItemRangeInserted(position + 1, childCount);
        }
        else {
            this.expanded.delete(groupPosition);
            notifyItemChanged(position);
            notifyItemRangeRemoved(position + 1, childCount);
        }
    }

    public final void toggleExpanded(int groupPosition) {
        setExpanded(groupPosition, !this.expanded.get(groupPosition));
    }

    protected abstract int getGroupCount();
    protected abstract GroupVH onCreateGroupViewHolder(LayoutInflater inflater, ViewGroup parent);
    protected abstract void onBindGroupViewHolder(GroupVH viewHolder, int groupPosition, boolean expanded);

    protected abstract int getChildCount(int groupPosition);
    protected abstract int getChildViewType(int groupPosition, int childPosition);
    protected abstract ChildVH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int childViewType);
    protected abstract void onBindViewHolder(ChildVH holder, int groupPosition, int childPosition);
}

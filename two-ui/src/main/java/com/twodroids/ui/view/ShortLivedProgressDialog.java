package com.twodroids.ui.view;


import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.twodroids.ui.R;

/**
 * A progress dialog that is likely to be visible for a very short time in optimal situations,
 * so avoid showing it at all in those cases.
 * Created by mensly on 12/08/2015.
 */
public class ShortLivedProgressDialog extends AlertDialog {
    private final long delay;
    private final long fade;
    public ShortLivedProgressDialog(Context context, String message) {
        super(context);
        Resources res = context.getResources();
        delay = res.getInteger(android.R.integer.config_longAnimTime);
        fade = res.getInteger(android.R.integer.config_shortAnimTime);
        setCancelable(false);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_short_lived_progress, null);
        TextView messageView = (TextView)view.findViewById(R.id.message);
        messageView.setText(message);
        setView(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        View decorView = getWindow().getDecorView();
        decorView.setAlpha(0);
        decorView.animate()
                .setStartDelay(delay)
                .alpha(1)
                .setDuration(fade)
                .start();
    }
}

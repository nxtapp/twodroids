package com.twodroids.common;


import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * A Handler that doesn't hold a strong reference to its Callback
 * Created by mensly on 20/01/2016.
 */
public final class WeakCallbackHandler extends Handler {
    private static final class WeakCallback implements Callback {
        private final WeakReference<Callback> callback;
        public WeakCallback(Handler.Callback callback) {
            this.callback = new WeakReference<>(callback);
        }

        @Override
        public boolean handleMessage(Message msg) {
            Handler.Callback callback = this.callback.get();
            return callback != null && callback.handleMessage(msg);
        }
    }

    public WeakCallbackHandler(Callback callback) {
        super(new WeakCallback(callback));
    }
}

package com.twodroids.common;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;

/**
 * Creator class to reduce boilerplate for implementing Parcelable
 * Created by mensly on 6/04/2016.
 */
public class SimpleParcelableCreator<T extends Parcelable> implements Parcelable.Creator<T> {
    private final Class<T> cls;
    private final Constructor<T> constructor;

    public SimpleParcelableCreator(Class<T> cls) {
        this.cls = cls;
        try {
            this.constructor = cls.getDeclaredConstructor(Parcel.class);
            this.constructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(
                    String.format("Class %s does not implement a constructor taking a Parcel as a single argument",
                            cls.getName()), e);
        }
    }

    @Override
    public T createFromParcel(Parcel source) {
        try {
            return constructor.newInstance(source);
        } catch (Exception e) {
            throw new RuntimeException("Error occurred in Parcelable constructor", e);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T[] newArray(int size) {
        return (T[]) Array.newInstance(cls, size);
    }
}
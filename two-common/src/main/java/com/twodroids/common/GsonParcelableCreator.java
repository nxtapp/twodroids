package com.twodroids.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.lang.reflect.Array;

/**
 * Creator class to reduce boilerplate for implementing Parcelable
 * Created by mensly on 6/04/2016.
 */
public final class GsonParcelableCreator<T extends Parcelable> implements Parcelable.Creator<T> {
    public static Gson GSON = new Gson();

    private final Class<T> cls;

    public GsonParcelableCreator(Class<T> cls) {
        this.cls = cls;
    }

    @Override
    public T createFromParcel(Parcel source) {
        return GSON.fromJson(source.readString(), cls);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T[] newArray(int size) {
        return (T[]) Array.newInstance(cls, size);
    }

    public static void writeToParcel(Parcelable object, Parcel dest, int flags) {
        dest.writeString(GSON.toJson(object));
    }
}

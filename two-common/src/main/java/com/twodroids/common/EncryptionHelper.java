package com.twodroids.common;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Simple interface for encryption with a simple PIN
 * Created by mensly on 14/08/2015.
 */
@SuppressWarnings("HardCodedStringLiteral")
public final class EncryptionHelper {

    public static final class EncryptionException extends Exception {
        public EncryptionException(Throwable cause) {
            super(cause);
        }
    }

    private EncryptionHelper() { }
    private static final int SALT_SIZE = 32;
    private static final int INIT_VECTOR_SIZE = 16;
    private static final int ITERATIONS = 1000;
    private static final int KEY_LENGTH = 256;

    private static Key generateKey(char[] pin, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(pin, salt, ITERATIONS, KEY_LENGTH);
        SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
        return new SecretKeySpec(secretKey.getEncoded(), "SHA1");
    }

    private static Cipher getCipherInstance(Key secretKey, int opMode, byte[] initializationVector)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(opMode, secretKey, new IvParameterSpec(initializationVector));
        return cipher;
    }

    public static String encrypt(String plainText, String pin) throws EncryptionException {
        if (plainText == null || pin == null) {
            return null;
        }
        try {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[SALT_SIZE];
            byte[] initializationVector = new byte[INIT_VECTOR_SIZE];
            random.nextBytes(salt);
            random.nextBytes(initializationVector);
            byte[] passwordData = plainText.getBytes("UTF-8");

            Key derivedKey = generateKey(pin.toCharArray(), salt);
            Cipher cipher = getCipherInstance(derivedKey, Cipher.ENCRYPT_MODE, initializationVector);
            byte[] cipherText = cipher.doFinal(passwordData);

            byte[] outputData = new byte[INIT_VECTOR_SIZE + SALT_SIZE + cipherText.length];
            System.arraycopy(initializationVector, 0, outputData, 0, INIT_VECTOR_SIZE);
            System.arraycopy(salt, 0, outputData, INIT_VECTOR_SIZE, SALT_SIZE);
            System.arraycopy(cipherText, 0, outputData, INIT_VECTOR_SIZE + SALT_SIZE, cipherText.length);
            return Base64.encodeToString(outputData, Base64.NO_WRAP);
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | InvalidAlgorithmParameterException |
                BadPaddingException | IllegalBlockSizeException | InvalidKeySpecException e) {
            // Invalid states should not occur outside of testing
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            throw new EncryptionException(e);
        }
    }

    public static String decrypt(String encrypted, String pin) throws EncryptionException {
        if (encrypted == null || pin == null) {
            return null;
        }
        try {
            byte[] data = Base64.decode(encrypted, Base64.NO_WRAP);
            if (data.length <= INIT_VECTOR_SIZE + SALT_SIZE) {
                throw new IllegalArgumentException("Insufficient data");
            }
            byte[] initializationVector = Arrays.copyOfRange(data, 0, INIT_VECTOR_SIZE);
            byte[] salt = Arrays.copyOfRange(data, INIT_VECTOR_SIZE, INIT_VECTOR_SIZE + SALT_SIZE);
            byte[] cipherText = Arrays.copyOfRange(data, INIT_VECTOR_SIZE + SALT_SIZE, data.length);

            Key secretKey = generateKey(pin.toCharArray(), salt);
            Cipher cipher = getCipherInstance(secretKey, Cipher.DECRYPT_MODE, initializationVector);
            try {
                byte[] plainText = cipher.doFinal(cipherText);
                return new String(plainText, "UTF-8");
            }
            catch (BadPaddingException e) {
                // Incorrect PIN
                return null;
            }
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | InvalidAlgorithmParameterException |
                IllegalBlockSizeException | InvalidKeySpecException e) {
            // Invalid states should not occur outside of testing
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            throw new EncryptionException(e);
        }
    }
}

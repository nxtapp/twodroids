package com.twodroids.common;

/**
 * Test class to check the build, checkstyle and test process.
 */
public final class TestClass {
    private TestClass() {
    }

    /**
     * The public constructor.
     *
     * @param someString The string to return
     */
    public TestClass(final String someString) {

    }

    /**
     * All public methods should be commented.
     *
     * @return and returning values described
     */
    public String getSomeString() {
        return "SomeString";
    }

    protected String getSomeProtectedString() {
        return "ProtectedString";
    }
}

package com.twodroids.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import java.lang.ref.WeakReference;

/**
 * Helper class simplifying pattern of registering a LocalBroadcastReceiver for a particular
 * IntentFilter and calling back in a memory-safe way.
 * Created by mensly on 20/01/2016.
 */
public abstract class LocalBroadcastReceiver<T> extends BroadcastReceiver {
    private final WeakReference<T> parent;
    private final IntentFilter intentFilter;

    protected LocalBroadcastReceiver(T parent, String ... actions) {
        this.parent = new WeakReference<>(parent);
        intentFilter = new IntentFilter();
        for (String action : actions) {
            intentFilter.addAction(action);
        }
    }

    protected LocalBroadcastReceiver(T parent, IntentFilter intentFilter) {
        this.parent = new WeakReference<>(parent);
        this.intentFilter = intentFilter;
    }

    public void register(Context context) {
        LocalBroadcastManager.getInstance(context).registerReceiver(this, intentFilter);
    }

    public void unregister(Context context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
    }

    @Override
    public final void onReceive(Context context, Intent intent) {
        T parent = this.parent.get();
        if (parent != null) {
            onReceive(parent, context, intent);
        }
    }

    protected abstract void onReceive(T parent, Context context, Intent intent);
}
